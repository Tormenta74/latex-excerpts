\documentclass[spanish]{article}

\usepackage[margin=3cm,top=2cm]{geometry}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{blindtext}
\usepackage{cancel}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{tikzscale}
\usepackage{tikz}
\usepackage{titlesec}
\usepackage{titling}
\usepackage{wrapfig}
\usepackage{xfrac}

% Personal package
\usepackage[neverindent]{coolmath}

\rhead{\small Listado de preguntas de teoría}
\chead{}
\lhead{\small Probabilidad II --- 2018/2019}
\lfoot{\scriptsize \leftmark}
\rfoot{\thepage}
\cfoot{\sc\scriptsize Matemáticas --- UAM}
\pagestyle{fancy}

\renewcommand\labelenumi{(\theenumi)}

\titleformat{\section}
{\Large\bf}
{}
{0em}
{}[\titlerule]

\usetikzlibrary{calc}
\usetikzlibrary{positioning}

\title{Listado de preguntas de teoría}
\date{\today}
\author{Diego Sáinz de Medrano}

\renewcommand{\maketitle}{\begin{center}{\Large\thetitle}\\\textsc{\large Probabilidad II}\end{center}}

\newcommand{\solucion}[1]{
\begin{center}
	\fbox{\parbox{0.9\textwidth}{%
		#1
	}}\\
\end{center}
}

\setlength{\fboxsep}{10pt}
\setlength{\fboxrule}{0.3pt}

\begin{document}

\thispagestyle{plain}
\maketitle

\section{Espacios de probabilidad}

Define $\sigma$-álgebra, $\pi$-sistema y $\lambda$-sistema. Enuncia el teorema de Dynkin, comenta brevemente su utilidad y menciona algún resultado en cuya demostración se utilice este teorema.\\

\solucion{%
	Para estos enunciados, sea $\Omega$ un conjunto no vacío.\\

	\definicion{sigma-algebra}{Una $\sigma$-álgebra $\mathcal{F} \subset \mathcal{P}(\Omega)$ es una clase de subconjuntos de $\Omega$ tal que
	\begin{itemize}
		\item $\Omega \in \mathcal{F}$
		\item $A\in\mathcal{F} \Rightarrow A^c \in \mathcal{F}$
		\item $A_n\in\mathcal{F} \quad \forall n \geq 1 \Rightarrow \bigcup_{\mathbb{N}} A_n \in \mathcal{F}$
	\end{itemize}
	\bitterend}

	\definicion{pi-algebra}{Un $\pi$-sistema $\mathcal{C} \subset \mathcal{P}(\Omega)$ es una clase de subconjuntos de $\Omega$ tal que
	$A,B \in \mathcal{C} \Rightarrow A\cup B\in\mathcal{C}$}

	\definicion{lambda-sistema}{Un $\lambda$-sistema $\mathcal{L} \subset \mathcal{P}(\Omega)$ es una clase de subconjuntos de $\Omega$ tal que
	\begin{itemize}
		\item $\Omega \in \mathcal{L}$
		\item $A\in\mathcal{L} \Rightarrow A^c \in \mathcal{L}$
		\item $A_n\in\mathcal{L}\quad\forall n\geq 1\;\wedge\; A_i\cap A_j\neq\emptyset\;\forall i\neq j\Rightarrow\bigcup_{\mathbb{N}} A_n\in\mathcal{L}$
	\end{itemize}
	\bitterend}

	\teorema{dynkin}{\textbf{Dynkin} Sean $\mathcal{C}$ un $\pi$-sistema y $\mathcal{L}$ un $\lambda$-sistema. Entonces
	\[
		\mathcal{C} \subset \mathcal{L} \Longrightarrow \sigma(\mathcal{C}) \subset \mathcal{L}
	\]\bitterend}

	La utilidad del teorema de Dynkin es que podemos reducir la cantidad de conjuntos que deben cumplir una propiedad para que la $\sigma$-álgebra de ellos
	también la cumpla, es decir: sea $\mathcal{L}$ una clase de conjuntos que cumplen una propiedad, y $\mathcal{C} \subset \mathcal{L}$ una subclase de ella.
	Si podemos ver que son $\lambda$ y $\pi$ sistemas respectivamente, tenemos que la $\sigma$-álgebra generada por $\mathcal{C}$ también cumple la propiedad (al
	estar dentro de $\mathcal{L}$). Se usa por ejemplo para demostrar el teorema del criterio básico de independencia (de $\sigma$-álgebras).
}

\pagebreak
Define límite superior y límite inferior de una sucesión de sucesos. Demuestra que
\begin{equation*}
P(\liminf A_n) \leq \liminf P(A_n) \leq \limsup P(A_n) \leq P(\limsup A_n)
\end{equation*}

\solucion{%
	Para estos enunciados, sea $A_n$ una sucesión de sucesos (conjuntos de una $\sigma$-álgebra $\mathcal{F}$).\\

	\definicion{limite-superior-sucesion-sucesos}{El límite superior de una sucesión de sucesos es
		\[
			\limsup A_n = \bigcap_{n=1}^{\infty} \bigcup_{k=n}^{\infty} A_k
		\]
	}

	\definicion{limite-inferior-sucesion-sucesos}{El límite inferior de una sucesión de sucesos es
		\[
			\liminf A_n = \bigcup_{n=1}^{\infty} \bigcap_{k=n}^{\infty} A_k
		\]
	}

	\demostracion{desigualdades-limites}{Primero vemos lo siguiente:
		\[
			P(\liminf A_n) \overset{\text{def}}{=} P(\bigcup_{n=1}^{\infty} \bigcap_{k=n}^{\infty} A_k) = \ulimit P(\bigcap_{k=n}^{\infty} A_k)
			\leq \liminf P(A_n)
		\]
		De forma similar,
		\[
			P(\limsup A_n) \overset{\text{def}}{=} P(\bigcap_{n=1}^{\infty} \bigcup_{k=n}^{\infty} A_k) = \ulimit P(\bigcup_{k=n}^{\infty} A_k)
			\geq \limsup P(A_n)
		\]
		Con lo que ya tenemos las desigualdades exteriores. Para la interior, basta entender la definición de los límites superior e inferior de suceciones
		numéricas, que por definición cumplen esta desigualdad.

		\hfill$\Box$
	}
}

\pagebreak
\section{Independencia}

Define la independencia de una familia de $\sigma$-álgebras. Enuncia y demuestra el teorema que da el criterio básico de independencia de $\sigma$-álgebras.\\

\solucion{%
	Para estos enunciados, será $(\Omega, \mathcal{F}, P)$ un espacio de probabilidad. Definimos $A,B\in\mathcal{F}$ independientes si y solo si $P(A\cap B) =
	P(A)P(B)$, y denotamos $AB := A\cap B$.\\

	\definicion{independencia-familia-sucesos}{%
		Sea una familia de clases de sucesos $\left\{\mathcal{C}_i\right\}_{i\in I}$. Se dice que esta familia es independiente si para cada elección $\left\{
		i_1, ..., i_k\right\} \subset I$ finita, los sucesos $\left\{C_i \in \mathcal{C}_i\right\}$ son independientes, es decir,
		$P(C_{i_1} ... C_{i_k}) =P(C_{i_1})...P(C_{i_k})$.
	}

	Esta definición se aplica sin más que cambiar el término ``clases de sucesos'' por ``$\sigma$-álgebras''.\\

	\teorema{criterio-basico-independencia}{\textbf{Criterio básico de independencia} Sean $\mathcal{C}_i \subset \mathcal{F},\;\; i=1,...,n \;\;\pi$-sistemas.
		Entonces las $\sigma$-álgebras que generan $\sigma(\mathcal{C}_i)$ son independientes.
	}

	\demostracion{criterio-basico-independencia}{%
		Supongamos sin pérdida de generalidad $\Omega \in \mathcal{C}_i$ (si no, consideraríamos las clases $\mathcal{C}'_i = \mathcal{C}_i \cup \Omega$, que
		seguirían siendo $\pi$-sistemas). Fijamos $A_j \in \mathcal{C}_j \;\; \forall j \geq 2$. Consideramos
		\[
			\mathcal{L} = \left\{A \in \mathcal{F}: P(AA_2...A_n) = P(A)P(A_2)...P(A_n)\right\}
		\]
		Tenemos que $\mathcal{C}_1, ..., \mathcal{C}_n$ independientes implica que $\mathcal{C}_1 \subset \mathcal{L}$: escogiendo cualquier conjunto de
		$\mathcal{C}_1$ e intersecándolo con cualquier colección de los otros $\mathcal{C}_i$, al ser independientes, tenemos que el cojunto seleccionado
		es independiente.\\

		Si tuviésemos que $\mathcal{L}$ es un $\lambda$-sistema, entonces, por Dynkin, tendríamos que $\sigma(\mathcal{C}_1) \subset \mathcal{L}$, luego
		$\sigma(\mathcal{C}_1), \mathcal{C}_2, ..., \mathcal{C}_n$ independientes, y realizando un argumento por inducción, habríamos terminado.\\

		Veamos que $\mathcal{L}$, efectivamente, cumple las condiciones de $\lambda$-sistema:

		\begin{itemize}
			\item ¿$\Omega \in \mathcal{L}$? \\
				$P(\Omega A_2...A_n) = P(A_2...A_n) = 1P(A_2)...P(A_n) = P(\Omega)P(A_2)...P(A_n)\quad\Box$
			\item ¿$A\in\mathcal{L} \Rightarrow A^c \in \mathcal{L}$? \\
				$P(A^c A_2...A_n) = P(A_2...A_n) - P(AA_2...A_n) = P(A_2...A_n)(1-P(A)) =$ \\
				$ = P(A_2...A_n)P(A^c) = P(A_2)...P(A_n)P(A^c) \Rightarrow A^c \in \mathcal{L} \quad\Box$
			\item ¿$A_n\in\mathcal{L}\quad\forall n\geq 1\;\wedge\; A_i\cap A_j\neq\emptyset\;\forall i\neq j\Rightarrow\bigcup_{\mathbb{N}} A_n\in\mathcal{L}$? \\
				Sea una sucesión $B_n \in \mathcal{L}$ con $B_iB_j = \emptyset\quad i\neq j$, y llamamos $B := \uplus_{\mathbb{N}} B_j$. Vemos que
				\[
					\begin{array}{r l}
						P(BA_2...A_n) & = P\left[\uplus_{\mathbb{N}}\left(B_kA_2...A_n\right)\right] \\
						& \overset{\text{disjuntos}}{=\joinrel=\joinrel=\joinrel=\joinrel=}
						\sum_{\mathbb{N}}P(B_k)P(A_2)...P(A_n) \\
						& \overset{\text{def}}{=\joinrel=} P(B)P(A_2)...P(A_n)
					\end{array}
				\]
				es decir, $B\in\mathcal{L} \quad\Box$
		\end{itemize}
		\hfill$\Box$
	}
}

\pagebreak
Define la $\sigma$-álgebra asintótica. Enuncia y demuestra la ley 0-1 de Kolmogorov.\\

\solucion{%
	Para estos enunciados, $\left\{\mathcal{F}_n\right\}$ es una sucesión de sub-$\sigma$-álgebras de $\mathcal{F}$. \\

	\definicion{sigma-algebra-asintotica}{Definimos las siguientes $\sigma$-álgebras:
		\[
			\mathcal{F}^{n} := \sigma \left(\bigcup_{k\geq n} \mathcal{F}_k\right)
		\]
		Estas $\sigma$-álgebras verifican $\mathcal{F}^i \supseteq \mathcal{F}^{i+1}\;\;\forall i$.
		\[
			\mathcal{F}^{\infty} := \bigcap_{n=0}^{\infty} \mathcal{F}^n = \bigcap_{n=0}^{\infty} \sigma \left(\bigcup_{k=n+1}^{\infty} \mathcal{F}_k\right)
		\]
		$\mathcal{F}^{\infty}$ se denomina la $\sigma$-álgebra asintótica relativa a $\left\{\mathcal{F}_n\right\}$.
	}

	\teorema{ley-01-kolmogorov}{\textbf{Ley 0-1 de Kolmogorov} Sean $\left\{\mathcal{F}_n\right\} \subset \mathcal{F}$ sub-$\sigma$-álgebras independientes.
	Entonces, $\forall A \in \mathcal{F}_{\infty}$, se tiene $P(A)=0$ ó $P(B)=1$.}

	\demostracion{ley-01-kolmogorov}{%
		\emph{Serede Probabilidad II, 2017}\\
		\[
			\begin{array}{r l l}
				(1)  & \mathcal{F}_1, ..., \mathcal{F}_n, \mathcal{F}_{n+1}, ... \text{ son independientes} & \text{(hipótesis)} \\
				(2)  & \mathcal{F}_1, ..., \mathcal{F}_n, \mathcal{F}^{n+1} \text{ son independientes} & \text{(teorema de agrupamiento)} \\
				(3)  & \mathcal{F}_1,...,\mathcal{F}_n,\mathcal{F}^{\infty}\text{ son independientes }\forall n & (\mathcal{F}^{\infty}\subset\mathcal{F}^{n+1}) \\
				(4)  & \mathcal{F}^{\infty}, ..., \mathcal{F}_n \text{ son independientes } \forall n & \\
				(5)  & \mathcal{F}^{\infty}, ..., \mathcal{F}_n, ... \text{ son independientes } & \text{(toda selección finita es independiente)} \\
				(6)  & \mathcal{F}^{\infty}, \mathcal{F}^1 \text{ son independientes} & \text{(teorema de agrupamiento)} \\
				(7)  & \mathcal{F}^{\infty}, \mathcal{F}^{\infty} \text{ son independientes} & (\mathcal{F}^1 \subset \mathcal{F}^{n+1})\\
				(8)  & A \in \mathcal{F}^{\infty} \Rightarrow A \text{ es independiente de } A & \\
				(9)  & P(AA) = P(A)P(A) & \text{(definición de independencia)} \\
				(10) & P(A) \in \left\{0, 1\right\}
			\end{array}
		\]
	}
}

\pagebreak
Enuncia y demuestra los lemas de Borel-Cantelli.\\

\solucion{%
	\lema{borel-cantelli-1}{\textbf{Primer lema de Borel-Cantelli}\\%
		Sea $\left\{A_n\right\} \subset \mathcal{F}$.
		\[
			\sum_{\mathbb{N}} P(A_n) < \infty \Longrightarrow P(\limsup A_n) = 0
		\]
	}

	\demostracion{borel-cantelli-1}{%
		Dado que la serie de las probabilidades converge, se tiene que dar que
		\[
			\exists N\in\mathbb{N} \text{ tal que }\sum_{n=N}^{\infty} P(A_n) = 0
		\]
		es decir, que
		\[
			\inf_{N\geq 1}\sum_{n=N}^{\infty} P(A_n) = 0
		\]
		y se sigue
		\[
			\begin{array}{r l}
				P(\limsup A_n) & = P(\text{se dan infinitos } A_n) \\
				& \overset{\text{def}}{=\joinrel=} P\left(\displaystyle\bigcap_{N=1}^{\infty} \bigcup_{n=N}^{\infty} A_n\right) \\
				& = \displaystyle\inf_{N \geq 1} P\left(\bigcup_{n=N}^{\infty} A_n\right) \\
				& \leq \displaystyle\inf_{N \geq 1} \sum_{n=N}^{\infty} P\left(A_n\right) \\
				& \overset{\text{arriba}}{=\joinrel=\joinrel=} 0 \quad\Box
			\end{array}
		\]
	}

	\lema{borel-cantelli-2}{\textbf{Segundo lema de Borel-Cantelli}\\%
		Sea $\left\{A_n\right\} \subset \mathcal{F}$ independientes.
		\[
			\sum_{\mathbb{N}} P(A_n) = \infty \Longrightarrow P(\limsup A_n) = 1
		\]
	}

	\demostracion{borel-cantelli-2-parte-1}{%
		Basta probar que la probabilidad de que se de $A_n$ para infinitos valores de $n$ es 0, es decir, probar que
		\[
			1 - P(\limsup A_n) = 0
		\]
		Desarrollamos y vemos:
		\[
			\begin{array}{r l}
				1 - P(\limsup A_n) & = 1 - P\left(\left(\displaystyle\bigcap_{N=1}^{\infty} \bigcup_{n=N}^{\infty} A_n\right)\right) \\
				& = P\left(\left(\displaystyle\bigcap_{N=1}^{\infty} \bigcup_{n=N}^{\infty} A_n\right)^c\right)
				  = P\left(\left(\displaystyle\bigcup_{N=1}^{\infty} \bigcap_{n=N}^{\infty} A_n^c\right)\right) \\
				& = P(\liminf A_n^c) = \limit{N}{\infty} P\left(\displaystyle\bigcap_{n=N}^{\infty}A_n^c\right)
			\end{array}
		\]
	}
}

\solucion{%
	\demostracion{borel-cantelli-2-parte-2}{\textit{(Continuación)}\\
		Ahora hay que ver que este último límite vale 0. Teniendo la independencia de los $A_n$:
		\[
			\begin{array}{r l}
				\limit{N}{\infty} P\left(\displaystyle\bigcap_{n=N}^{\infty}A_n^c\right) & = \displaystyle\prod_{n=N}^{\infty}P(A_n^c) \\
				& = \displaystyle\prod_{n=N}^{\infty}\left(1-P(A_n)\right) \\
				& = \displaystyle\prod_{n=N}^{\infty}\exp\left(-P(A_n)\right) \\
				& \leq \exp\left(\displaystyle\sum_{n=N}^{\infty}-P(A_n)\right) \\
				& = 0 \quad\text{(por hipótesis, esta serie diverge)}\quad\Box
			\end{array}
		\]
		De ambos hechos, deducimos el resultado del segundo lema.\hfill$\Box$
	}
}

\pagebreak
Define qué es la $\sigma$-álgebra asociada a una familia de variables aleatorias. Define la independencia de una familia de variables aleatorias y da alguna caracterización sencilla.\\

\solucion{%
	En estos enunciados, $(\Omega, \mathcal{F}, P)$ será un espacio de probabilidad y $(\mathbb{R},\mathcal{B})$ es la recta real con la topología boreliana. Lo
	definimos en un momento, pero también denotaremos $\mathcal{X}:=\left\{X_i:i\in I\right\}$ como una familia de variables aleatorias.\\

	\textit{(Definiciones útiles para construir el concepto, pero no nos las piden):}\\
	\definicion{sigma-algebra-generada-variable-aleatoria}{%
		Sea $X:(\Omega,\mathcal{F}) \longrightarrow (\mathbb{R},\mathcal{B})$ una variable aleatoria. La clase de conjuntos
		\[
			\sigma(X) = \left\{X^{-1}(B):B\in\mathcal{B}(\mathbb{R})\right\}
		\]
		es una sub-$\sigma$-álgebra de $\mathcal{F}$ denominada $\sigma$-álgebra generada por X.
	}

	\definicion{sigma-algebra-generada-familia-va}{%
		Sea $\mathcal{X}$ una familia de variables aleatorias. La clase de conjuntos
		\[
			\sigma(\mathcal{X}) = \sigma\left(\bigcup_{i\in I} \sigma(X_i)\right)
		\]
		se denomina $\sigma$-álgebra generada por la familia de variables aleatorias $X_i$.
	}

	\definicion{funciones-distribucion-finito-dimensionales}{%
		Sea $\mathcal{X}$ una familia de variables aleatorias. Las funciones
		\[
			F_J(x_j, j\in J) := P\left\{X_j \leq x_j, j\in J\right\}
		\]
		para todos los subconjuntos finitos $J\subset I$ se denominan funciones de distribución finito dimensionales.
	}

	\textit{(Relacionado, pero no nos lo piden):}\\
	\proposicion{minima-sigma-algebra-va}{%
		$\mathcal{X}$ es la mínima $\sigma$-álgebra que hace medible simultáneamente todas las variables aleatorias de la familia medibles.
	}

	\definicion{independencia-familia-va}{%
		Sea $\mathcal{X}$ una familia de variables aleatorias. Se dice que la familia es de variables independientes si las $\sigma$-álgebras que generan
		las variables de la familia son independientes.
	}

	\proposicion{criterio-factorizacion-independencia}{\textbf{Criterio de factorización}\\
		Una familia de v.a. $\mathcal{X}$ es independiente si y solo si para todo subconjunto finito $J \subset I$ se da que
		\[
			F_J(x_j, j\in J) = \prod_{j\in J}P(X_j \leq x_j)
		\]
	\bitterend}

	\proposicion{independencia-familia-finita-va}{%
		Las variables aleatorias $X_1, ..., X_n$ son aleatorias si y solo si
		\[
			P(X_1 \leq x_1, ...,X_n \leq x_n) = \prod_{i=1}^{n} P(X_j \leq x_j)
		\]
	\bitterend}
}

\pagebreak
\section{Esperanza}

Enuncia y demuestra el teorema de cambio de espacio de integración.\\

\solucion{%
	\teorema{cambio-espacio-integracion}{\textbf{Cambio de espacio de integración}\\%
		Sea $X$ una variable aleatoria sobre $(\Omega,\mathcal{F},P)$ con función de distribución $F_X$. Sea $g:\mathbb{R} \longrightarrow \mathbb{R}$ medible
		de Borel. Si $Y = g(X)$, entonces
		\[
			E(Y) = \int_{\mathbb{R}} g(x)dF_X(x) = \int_{\mathbb{R}} gdP_X
		\]
	}

	\demostracion{cambio-espacio-integracion}{Se utiliza el método de escala ascendente, es decir, se demuestra para:
		\begin{enumerate}
			\item Indicadores $g=I_B$\\
				<++> Apuntes de clase
			\item Funciones simples no negativas, $g=\sum_{i=1}^{n} x_i I_B$ (linealidad)\\
				<++> Apuntes de clase
			\item Funciones no negativas, $g \geq 0$ (TCM)\\
				<++> Apuntes de clase
			\item Funciones borelianas arbitrarias $g$ ($g = g^+ - g^-$)\\
				<++> Apuntes de clase
		\end{enumerate}
	\bitterend}
}

Enuncia y demuestra las desigualdades de Markov y Chebychev\\

\solucion{%
	\proposicion{desigualdad-markov}{\textbf{Desigualdad de Markov}\\%
		Sea $X \geq 0$ y $\epsilon > 0$. Entonces
		\[
			P(X \geq \epsilon) \leq \frac{E(X)}{\epsilon}
		\]
	\bitterend}

	\demostracion{desigualdad-markov}{%
		\emph{Serede Probabilidad II, 2017}${}^\ast$\\
		De hecho probamos lo siguiente (llamémoslo desigualdad de Markov${}^\ast$): 
		\[
			P(|X| \geq \epsilon) \leq \frac{E|X|^{\alpha}}{\epsilon^\alpha}
		\]

		\[
			E|X|^\alpha = \int_{\Omega} |X|^\alpha dP = \int_{\left\{|X| \geq \epsilon\right\}} |X|^\alpha dP
			+ \int_{\left\{|X| < \epsilon\right\}} |X|^\alpha dP \geq \int_{\left\{|X| \geq \epsilon\right\}} |X|^\alpha dP
			\geq \epsilon^\alpha P(|X|\geq \epsilon)
		\]
		Y lo aplicamos para $X$ no negativa y $\alpha = 1 \quad \Box$
	}

	\proposicion{desigualdad-chebychev}{\textbf{Desigualdad de Chebychev}\\%
		Sea $X \in L_1$ y $\epsilon > 0$. Entonces
		\[
			P(|X - E(X)| \geq \epsilon) \leq \frac{V(X)}{\epsilon^2}
		\]
	\bitterend}

	\demostracion{desigualdad-chebychev}{%
		\emph{Serede Probabilidad II, 2017}\\
		Aplicamos Markov${}^\ast$ con $Y = X - E(X)$ y $\alpha = 2 \quad \Box$
	}
}

Enuncia y demuestra las desigualdades de Jensen y Lyapunov.\\

\solucion{%
	\proposicion{desigualdad-jensen}{\textbf{Desigualdad de Jensen --- Funciones convexas}\\%
		Sea $f:\mathbb{R} \longrightarrow \mathbb{R}$ convexa, $f, X \in L_1$. Entonces
		\[
			f\left[E(X)\right] \leq E\left[f(X)\right]
		\]
	\bitterend}

	\demostracion{desigualdad-jensen}{%
		\emph{https://planetmath.org/proofofjensensinequality}\\
		Sea $c=E(X)$, garantizado porque $X\in L_1$. Dado que $f$ es convexa, vemos que
		\[
			\exists \alpha \in \mathbb{R} \text{ tal que } \varphi(x) = \alpha (x - c) + f(c) \leq f(x)
		\]
		Entonces
		\[
			E(f(X)) \geq E(\varphi(X)) = E(\alpha(X - E(X)) + f(E(X))) = \cancel{E(\alpha(X - E(X)))} + f(E(X))
		\]
		\hfill$\Box$
	}

	\proposicion{desigualdad-lyapunov}{\textbf{Lyapunov}\\%
		Sea $0 < s < t$. Entonces
		\[
			(E|X|^s)^{\sfrac{1}{s}} \leq (E|X|^t)^{\sfrac{1}{t}}
		\]
	\bitterend}

	\demostracion{desigualdad-lyapunov}{%
		<++> ???
	}
}

\pagebreak
\section{La función característica}

Demuestra la fórmula de inversión para variables aleatorias con valores enteros. \textbf{Cayó en el control de 2019.}\\

\solucion{%
	\proposicion{formula-inversion-enteros}{\textbf{Fórmula de inversión (funciones con valores enteros)}\\%
		Sea $X$ una v.a. con función de distribución $F$ y función característica $\varphi$, con valores en $\mathbb{Z}$.  Entonces, para todo $n\in\mathbb{Z}$
		\[
			P(X = n) = \frac{1}{2\pi} \int_{-\pi}^{\pi} e^{-itn} \varphi(t)dt
		\]
	}

	\demostracion{formula-inversion-enteros}{%
		\emph{Serede Probabilidad II, 2017}\\
		\[
			\begin{array}{r l}
				\displaystyle\frac{1}{2\pi} \int_{-\pi}^{\pi} e^{-itn} \varphi(t)dt
				&\displaystyle = \frac{1}{2\pi}\int_{-\pi}^{\pi} e^{-itn} \left(\int_{\Omega}e^{itX}dP\right)dt\\
				&\displaystyle \overset{\ast}{=} \frac{1}{2\pi} \int_{\Omega}\left(\int_{-\pi}^{\pi}e^{it(X-n)}dt\right)dP \\
				&\displaystyle\overset{\ast\ast}{=\joinrel=}\frac{1}{2\pi}\sum_{m\in\mathbb{Z}}\int_{\left\{X=m\right\}}\left(\int_{-\pi}^{\pi}e^{it(m-n)}dt\right)dP \\
				&\displaystyle \overset{\ast\ast\ast}{=\joinrel=} \int_{\left\{X=m\right\}} dP = P(X = n)
			\end{array}
		\]
		Donde
		\[
			\begin{array}{r l}
				(\ast) & |e^{it(X-n)}| = 1 \in L_1([-\pi,\pi]\times\Omega), \displaystyle\int_{-\pi}^{\pi} \int_{\Omega}dP dt = 2\pi < \infty \\
				(\ast\ast) & \Omega = \displaystyle\uplus_{m\in\mathbb{Z}}\left\{X=m\right\} \\
				(\ast\ast\ast) & m \neq n \Rightarrow \displaystyle\int_{-\pi}^{\pi}e^{it(m-n)}dt = \frac{1}{m-n} e^{it(m-n)}\big|_{-\pi}^\pi = 0
			\end{array}
		\]
		\hfill$\Box$
	}
}

\pagebreak
Demuestra que si E$|X|<\infty$, entonces la derivada de la función característica de $X$ es \\$\varphi'(t) = \text{E}(iXe^{itX})$.\\

\solucion{%
	Este resultado está dominado por el teorema de derivación bajo el signo de la integral.\\

	\demostracion{derivacion-bajo-signo-integral}{
		Escribimos la definición de la función característica de $X$:
		\[
			\varphi_X(t) = E(e^{itX}) = \int_{\mathbb{R}} e^{itx}dF_X(x) \overset{\text{TCEI}}{=} \int_{\Omega} e^{itX(w)}dP(w)
		\]
		Definimos
		\[
			f(t,w) := e^{itX(w)}
		\]
		$f$ verifica
		\begin{itemize}
			\item $\forall t \in I \subset \mathbb{R} \;\; f(t,\cdot):\Omega \longrightarrow \mathbb{C}$ es integrable
			\item $\forall w \in \Omega \;\; f(\cdot, w):I \longrightarrow \mathbb{C}$ es derivable
		\end{itemize}

		Calculamos su derivada:
		\[
			\frac{\partial}{\partial t}f(t,w) = iX(w)e^{itX(w)}
		\]
		y vemos que
		\[
			\left|\frac{\partial}{\partial t}f(t,w)\right| = \left|iX(w)e^{itX(w)}\right| = |X(w)| \in L_1 \text{ (es decir, integrable)}
		\]
		luego podemos aplicar el resultado de la derivación bajo el signo integral, ya que $E|X| < \infty$, luego $\exists \varphi'(t)$ y es igual a
		\[
			\varphi'(t) = \int_{\Omega} iXe^{itX}dP
		\]
	}
}

Sea $\varphi(t) = \exp\{it'\mu−(1/2)t'\Sigma t\}$, donde $\mu,t \in \mathbb{R}^n$ y $\Sigma$ es una matriz $n\times n$ simétrica y semidefinida positiva. Demuestra que existe un vector aleatorio $X$ cuya función característica es $\varphi$. Definición de la distribución normal multivariante.\\

\solucion{%
<++>
}

\pagebreak
\section{Modos de convergencia y LGN}

Definición de los modos de convergencia. Demuestra que si hay convergencia completa, entonces hay convergencia casi segura.

\solucion{%
	\definicion{metodos-convergencia}{%
		\[
			X_n \overset{\text{c.s.}}{\longrightarrow} X \Longleftrightarrow
			P(X_n \to X) = 1
		\]
		\[
			X_n \overset{\text{P}}{\longrightarrow} X \Longleftrightarrow
			\forall \varepsilon > 0, P(|X_n - X| \geq \varepsilon) \to 0
		\]
		\[
			X_n \overset{m-p}{\longrightarrow} X \Longleftrightarrow
			E|x_n - X|^p \to 0 (p > 0)
		\]
		\[
			X_n \overset{\text{d}}{\longrightarrow} X \Longleftrightarrow
			F_n(x) \to F(x) \forall x \in \mathcal{C}_F
		\]
		\bitterend
	}

	\demostracion{completa-implica-casi-segura}{%
		<++>
	}
}

Definición de los modos de convergencia. Demuestra que si hay convergencia en probabilidad, existe una subsucesión que converge casi seguramente.

\solucion{%
	\demostracion{convergencia-probabilidad-implica-subsucesion-casi-segura}{%
		Sea $\left\{X'_n\right\}$ una subsucesión de $X_n$. Se tiene que
		$X_n \overset{\text{P}}{\longrightarrow} X$. Sea $\varepsilon > 0$
		$\forall k \in \mathbb{N} \; \exists n_k \in \mathbb{N}$ tal que
		$P(|X'_{n_k} - X| \geq \varepsilon) \leq \frac{1}{2^k}$ (porque la
		sucesión original converge en probabilidad, también lo harán las
		subsuseciones). Como
		\[
			\sum_{\mathbb{N}} P(|X'_{n_k} - X| \geq \varepsilon) \leq
			\sum_{\mathbb{N}} \frac{1}{2^k} < \infty
		\]
		entonces $X'_n \overset{\text{c.s.}}{\longrightarrow} X$.$\quad\Box$
	}
}

Definición de los modos de convergencia. Demuestra que la convergencia en probabilidad implica la convergencia en distribución. Demuestra que ambas son equivalentes en caso de que el límite sea degenerado.

\solucion{%
	\demostracion{probabilidad-implica-distribucion}{%
		Sea $F_n$ la función de distribución de $X_n$ y $F$ la función de
		distribución de $X$.\\
		Sea $\varepsilon > 0$. Se tiene (1):
		\begin{gather*}
			F_n(x) = P(X_n \leq x) = P(X_n \leq x, |X_n-X| < \varepsilon) +
			P(X_n \leq x, |X_n-X| \geq \varepsilon) \leq \\
			P(X \leq x + \varepsilon) + P(|X_n - X| \geq \varepsilon)
		\end{gather*}
		Por otro lado, (2):
		\begin{gather*}
			F(x-\varepsilon) = P(X \leq x + \varepsilon, |X_n-X|<\varepsilon) +
			P(X \leq x + \varepsilon, |X_n-X| \geq \varepsilon) \leq \\
			F_n(x) + P(|X_n - X| \geq \varepsilon)
		\end{gather*}
		Y ahora
		\[
			F(x-\varepsilon) \overset{(2)}{\leq} \liminf F_n(x)
			\overset{\text{def}}{\leq} \limsup F_n(x) \overset{(1)}{\leq}
			F(x+\varepsilon)
		\]
		Si $x\in\mathcal{C}_F$ y $\varepsilon \to 0^+$, entonces $F(x) =
		\ulimit F_n(x) \Rightarrow X_n \overset{\text{d}}{\longrightarrow} X$.
		$\quad\Box$
	}
}

Enuncia y demuestra la LFGN de Cantelli para v.a. independientes y con momento de cuarto orden uniformemente acotado.

\solucion{%
	\teorema{lfgn-cantelli-ind-cuarto-momento-acotado}{%
		Sea $\left\{X_n\right\}$ una sucesión de variables aleatorias
		independientes tal que $E(X_n^4)$ es finito y
		$E|X_n - E(X_n)|^4 \leq cte \; \forall n \geq 1$. Entonces
		\[
			\frac{S_n - E(S_n)}{n} \overset{\text{c.s.}}{\longrightarrow} 0
		\]
		\bitterend
	}

	\demostracion{lfgn-cantelli-ind-cuarto-momento-acotado}{%
		Sin pérdida de generalidad, asumimos que $E(X_n) = 0$ (si no,
		consideramos las variables $Y_n = X_n - E(X_n)$). Tenemos que probar
		$\frac{S_n}{n} \overset{\text{c.s.}}{\longrightarrow} 0$. Basta ver
		que $\forall \varepsilon > 0 \; \displaystyle\sum_{\mathbb{N}}
		P(|\frac{S_n}{n}| > \varepsilon) < \infty$. Se tiene
		\[
			P\left(\left|\frac{S_n}{n}\right| > \varepsilon\right) =
			P\left(\left|\frac{S_n^4}{n^4}\right| >
			\varepsilon^4\right) \overset{\text{Markov}}{\leq}
			\frac{E(S_n^4)}{n^4\varepsilon^4} \quad(\ast)
		\]
		es decir, basta ver $\sum(\ast) < \infty$.
		Tenemos
		\[
			E(S_n^4) = E\left[(X_1 + ... + X_n)^4\right] = \sum_{i=1}^n
			E(X_i^4) + \begin{pmatrix}4 \\ 2\end{pmatrix}
				\sum_{i<j}E(X_i^2X_j^2) + \text{resto} \quad(\ast\ast)
		\]
		$E$(resto) = 0, porque siempre que aparece un factor $\cancel{E(X_i)}$
		algo = 0, ya que las variables son independientes.

		$E(X_i^2X_j^2) = E(X_i^2)E(X_j^2) \overset{\text{Lyapunov}}{\leq}
		E(X_i^4)^{1/2}E(X_j^4)^{1/2} \overset{\text{hipótesis}}{\leq} C$,
		para algún $C < \infty$. Entonces
		\[
			(\ast\ast) \leq nC + 6\begin{pmatrix}n\\2\end{pmatrix}C = ... =
			(3n^2 - 2n)C \leq 3n^2 C
		\]
		Ya hemos terminado, porque recuperamos las desigualdades:
		\[
			P\left(\left|\frac{S_n^4}{n^4}\right| > \varepsilon^4\right) \leq
			\frac{E(S_n^4)}{n^3\varepsilon^4} \leq
			\frac{3\cancel{n^2} C}{n^{\cancel{4}\,2} \varepsilon^4}
		\]
		cuya suma es finita.$\quad\Box$
	}
}

\pagebreak
Enuncia las dos LFGN de Kolmogorov (caso i.i.d. y caso de v.a. independientes con varianza finita)

\solucion{%
	\teorema{lfgn-kolmogorov-iid}{\textbf{LFGN de Kolmogorov}\\
		Sea $\left\{X_n\right\}$ una sucesión de variables aleatorias
		independientes idénticamente distribuidas tal que $E|X_n^4| = \mu$ es
		finito. Entonces
		\[
			\frac{S_n - E(S_n)}{n} \overset{\text{c.s.}}{\longrightarrow} \mu
		\]
		\bitterend
	}

	\teorema{lfgn-kolmogorov-independientes-varianza-finita}{
		\textbf{LFGN de Kolmogorov (varianza finita)}\\
		Sea $\left\{X_n\right\}$ una sucesión de variables aleatorias
		independientes con varianza finita. Sea \\
		$0 < b_n \uparrow \infty$.
		Entonces
		\[
			\sum_{\mathbb{N}}\frac{V(X_n)}{b_n^2} < \infty \Longrightarrow
			\frac{S_n - E(S_n)}{b_n} \overset{\text{c.s.}}{\longrightarrow} 0
		\]
		\bitterend
	}
}

\pagebreak
\section{Convergencia en distribución y teorema central del límite}

Enuncia el teorema de continuidad de Lévy. Aplicando este teorema demuestra el TCL para v.a.i.i.d.

\solucion{%
<++>
}

Enuncia y demuestra el resultado conocido como método delta.

\solucion{%
<++>
}

Enuncia el TCL de Lindeberg-Feller y comenta brevemente su aplicación al caso del modelo de regresión lineal simple.

\solucion{%
<++>
}

\pagebreak
\section{Esperanza condicionada}

Define el concepto de esperanza de una v.a. condicionada a una $\sigma$-álgebra. Explica por qué existe y es única c.s.

\solucion{%
<++>
}

Demuestra que la esperanza de una v.a. $Y\in L_2$ condicionada a una $\sigma$-álgebra $\mathcal{D}$ es la proyección de $Y$ sobre el subespacio de v.a. medibles respecto a $\mathcal{D}$ y de cuadrado integrable. Comenta brevemente el significado intuitivo de este resultado.

\solucion{%
<++>
}

\end{document}
