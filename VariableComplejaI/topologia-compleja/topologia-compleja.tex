\documentclass[spanish]{article}

\usepackage[margin=3cm,top=1cm]{geometry}
\usepackage{blindtext}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage{titling}
\usepackage{titlesec}
\usepackage{wrapfig}

% Personal package
\usepackage[neverindent]{coolmath}

\renewcommand\labelenumi{(\theenumi)}

\titleformat{\subsection}
{\large\bf}
{}
{0em}
{}[\titlerule]

\usetikzlibrary{calc}
\usetikzlibrary{positioning}

\title{Topología Compleja}
\date{11 febrero 2019}
\author{Diego Sáinz de Medrano}

\renewcommand{\maketitle}{ \begin{center} {\Large\thetitle} \\ \thedate \end{center} }

\begin{document}

\maketitle

\section{Algunos conjuntos en el plano.}
\subsection{Circunferencia}

\definicion{circunferencia}{Lugar geométrico de los puntos que distan de un
centro $a$ una cantidad real positiva $r$.}

\begin{wrapfigure}{l}{0.3\textwidth}
\vspace{-15pt}
\centering
\includegraphics[height=3cm]{circunferencia}
\caption{$z \in c_a(r)$}
\vspace{-70pt}
\end{wrapfigure}

Escribimos $C_a(r)$ para denotar la circunferencia de centro $a$ y radio
$r > 0$. Si $a = x_0 + i y_0$ y $z =x + i y$:
\begin{gather*}
    z \in C_a(r) \\
    \Updownarrow \\
    | z - a | = r \\
    \Updownarrow \\
    (x - x_0)^2 + (y - y_0)^2 = r
\end{gather*}

\bigskip

\subsection{Disco}

\bigskip

\definicion{disco-abierto}{Un \textbf{disco abierto} es el interior de una
circunferencia. Denotamos el disco de centro $c$ y radio $r$ por $D(c;r)
= \left\{z:\;|z-a| < r\right\}$.}

\definicion{disco-cerrado}{Un \textbf{disco cerrado} es el cierre de un disco
abierto. Denotamos el disco cerrado de centro $c$ y radio $r$ por $\bar{D}(c;r)
= \left\{z:\;|z-a| \leq r\right\}$.}

\subsection{Rectas}
\begin{minipage}{0.45\textwidth}
\vspace{-81pt}
\subsubsection{Rectas verticales}

Definimos una recta vertical en $\mathbb{R}^2$ por la ecuación $x=a$. Para
un número complejo cualquiera $z = x + iy$, podremos reescribir la ecuación
como $$\real z = a$$.

\begin{wrapfigure}{l}{\textwidth}
\vspace{-25pt}
\centering
\includegraphics[height=4cm]{rectas}
\caption{Rectas}
\vspace{-70pt}
\end{wrapfigure}
\hspace{0pt}\\
\end{minipage}\hspace{0.1\textwidth}
\begin{minipage}{0.45\textwidth}
\subsubsection{Rectas oblicuas}

Las rectas con pendiente finita, es decir, oblicuas u horizontales, se pueden
describir con la ecuación tradicional $y = mx + n$.

\observacion{$-i z = -i(x + i y) = y -ix$, y por tanto, $y = \imag z =
\real \left\{-i z\right\}$.}

Reescribimos la ecuación:
\begin{gather*}
    \real\left\{-i z\right\} = m \real z + n,\quad m,n \in \mathbb{R}\\
    = \real\left\{m z + n\right\}\\
    \Updownarrow \\
    \real\left\{m z + i z + n\right\} = 0\\
    \Updownarrow \\
    \real\left\{z(m + i) + n\right\} = 0\\
\end{gather*}
\end{minipage}

\newpage
\subsection{Semiplanos}

\begingroup
Un semiplano $\Omega \subseteq \mathbb{C}$ viene dado por la ecuación:
$\Omega = \left\{x + i y : f(x, y) > 0, f \text{ lineal}\right\}$

\begin{wrapfigure}{l}{\textwidth}
\centering
\includegraphics[height=5cm]{semiplano}
\caption{$\Omega = \left\{z: x>a\right\}$}
\vspace{-70pt}
\end{wrapfigure}
\hspace{0pt}\\

\endgroup

\vspace{6cm}

\section{Convergencia de sucesiones en $\mathbb{C}$}

\definicion{sucesion-c}{Una \textbf{sucesión infinita} en $\mathbb{C}$ es una
función $z:\mathbb{N} \longrightarrow \mathbb{C}$.}
\notacion{$\forall n \in \mathbb{N},\; z(n) = z_n = z_n + i y_n$.}

\definicion{limite}{Llamamos a $z_0$ el \textbf{límite} de $z_n$
($\limit{n}{\infty} z_n = z_0$)
si $|z_n - z_0| \shortlimit{n}{\infty}0$.}

\proposicion{limite-c-por-limites-r}{\begin{equation}
    z_n \limit{n}{\infty} z_0 \Leftrightarrow \begin{array}{c}
    x_n \longrightarrow x_0 \\ y_n \longrightarrow y_0
\end{array} n \rightarrow \infty
\end{equation}\bitterend}

\ejemplo{sucesion-convergente}{La siguiente sucesión tiene límite: $$z_n =
\left(5+\frac{1}{n}\right)^2 + i\left(1 - \frac{1}{n}\right)^n
\shortlimit{n}{\infty}25 + i\frac{1}{e}$$ viendo que las partes real e
imaginaria tienen límite respectivamente.}

\proposicion{propiedades-limite}{\textbf{Propiedades del límite}. Si
$\limit{n}{\infty} z_n = z_0$ y $\limit{n}{\infty} w_n = w_0$,
$\lambda\in\mathbb{R}$, entonces:
\begin{enumerate}
    \item $\ulimit (z_n + w_n) = z_0 + w_0$
    \item $\ulimit (\lambda z_n) = \lambda z_0$
    \item $\ulimit (z_n w_n) = z_0 w_0$
    \item $\ulimit (\frac{z_n}{w_n}) = \frac{z_0}{w_0}$ \quad \textbf{ssi} $w_0 \neq 0$
    \item $\ulimit |z_n| = |z_0|$ $(\ast)$
    \item $z_n\shortulimit 0 \Leftrightarrow |z_n| \shortulimit 0$ $(\ast\ast)$
\end{enumerate}\bitterend}

\newpage
\demostracion{prop-5}{($\ast$)\begin{itemize}
    \item $\big| |z_n| - |z_0| \big| \leq |z_n - z_0|$
    \item $0 \leq \big| |z_n| - |z_0| \big| \leq |z_n - z_0|$
    \item $0 \shortulimit 0 \land |z_n - z_0| \shortulimit 0$, por hipótesis
    \item Aplicamos el lema del sándwich.
\end{itemize}\bitterend}

\demostracion{prop-6}{($\ast\ast$)
\begin{gather*}
    z_n = x_n + i y_n \shortulimit 0 \\
    \Updownarrow \text{\scriptsize proposición (1)} \\
    x_n \shortulimit 0 \quad\land\quad y_n \shortulimit 0 \\
    \Updownarrow \text{\scriptsize props lím} \\
    x_n^2 + y_n^2 \shortulimit 0 \\
    \Updownarrow \text{\scriptsize def} \\
    |z_n| \shortulimit 0
\end{gather*}\bitterend}

\section{Conjuntos abiertos y cerrados en $\mathbb{C}$}
\definicion{abierto}{$\Omega\subset\mathbb{C}$ es un \textbf{abierto} si
$\forall z \in \Omega \exists r > 0 : D(z;r) \subseteq \Omega$.}

\ejemplo{abiertos}{$\emptyset$, $\mathbb{C}$, discos abiertos, semiplanos.}

\definicion{cerrado}{$F \subset\mathbb{C}$ es un \textbf{cerrado} si
$\forall (z_n)_{n=1}^{\infty} \in F$ con $z_n \shortulimit z_0 \Rightarrow z_o \in F$.}

\proposicion{abierto-cerrado-complementario}{$F$ es cerrado $\Leftrightarrow
F^c := \mathbb{C}\setminus F$ es abierto.}

\proposicion{c-completo}{$\left(\mathbb{C},d(z,w) = |z-w|\right)$ es un
\textbf{espacio métrico completo}.}

\definicion{cierre}{$\boldsymbol{\bar{A}}$, el \textbf{cierre} de $A$:\\

\begin{array}{r c l}
    z \in \bar{A} & \Longleftrightarrow & \forall r > 0 \; D(z;r) \cap A \neq \emptyset \\
                  & \Longleftrightarrow & \exists (z_n)_{n=1}^{\infty} \subset A \text{ tal que } z_n \shortulimit z
\end{array}
}

\definicion{frontera}{$\boldsymbol{\partial A}$, la \textbf{frontera} de $A$:\\

\begin{array}{r c l}
    z \in \partial A & \Longleftrightarrow & \forall r > 0 \; D(z;r) \cap A \neq \emptyset \;\land\; D(z;r) \cap A^c \neq \emptyset \\
                     & \Longleftrightarrow & \exists (z_n)_{n=1}^{\infty} \subset A \;\land\; \exists (w_n)_{n=1}^{\infty} \subset A^c \text{ tal que } \ulimit z_n = \ulimit w_n = z
\end{array}
}

\definicion{interior}{$\boldsymbol{\overset{\circ}{A}}$, el \textbf{interior} de
$A$:\\

\hspace{6pt}$z \in \bar{A} \Longleftrightarrow \exists r > 0 \text{ tal que } D(z;r) \subseteq A$
}

\definicion{dominio}{$\Omega \subseteq \mathbb{C}$ es un \textbf{dominio} si
es un conjunto abierto y conexo.}

\teorema{dominios-conexos-caminos}{Todo $\Omega$ dominio en $\mathbb{C}$ es
conexo por caminos, y además, $\forall z,w \in \Omega \; \exists$ una línea
poligonal que une $z$ con $w$ y cuyos segmentos son paralelos a los ejes.}

\definicion{simplemente-conexo}{$\Omega \subseteq \mathbb{C}$ es
\textbf{simplemente conexo} si es un dominio (se sigue inmediatamente del
teorema anterior).}

\proposicion{topologia-algebraica}{(Resultado de Topología) Para todo $\Omega$
dominio simplemente conexo en $\mathbb{C}$, el grupo fundamental $\Pi_1(\Omega,
z),\;(z\in\Omega)$ no depende del punto base.}

\section{Compacidad}

\definicion{recubrimiento}{Dado un conjunto $K$, un \textbf{recubrimiento} de $K$
(por abiertos) es una familia $\left\{A_i\right\}_{i\in I}$ tal que
$K \subset \underset{i\in I}{\cup}A_i$.}

\definicion{compacidad}{Un conjunto $K$ es \textbf{compacto} si para todo
recubrimiento de $K$ $\left\{A_i\right\}_{i\in I}$ existe un subrecubrimiento
finito de $K$.}

\teorema{compacto-cerrado-acotado}{Un conjunto $K \subset \mathbb{C}$ es
compacto si es cerrado y acotado.}

\proposicion{compacto-subsucesiones}{Un conjunto $K \subset \mathbb{C}$ es
compacto si y solo si $\forall (z_n)_{n=1}^{\infty}$ sucesión en $K$ $\exists
(z_{n_j})_{j=1}^{\infty}$ subsucesión de $(z_n)$ tal que $z_{n_j}
\shortlimit{j}{\infty} z_0 \Rightarrow z_0 \in K$.}

\end{document}